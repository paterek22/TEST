Banner se nezobrazuje na mobilních zařízeních kvůli CSS.
Konkrétně kvůli funkci @media, kdy máte nastavené že do šířky 800px se banner, ani odstavec nad ním, nebudou zobrazovat. Pokud změníme display: none; na display: block, již by se měl banner
zobrazovat ve spodní části webu společně se sekcí, která se nachází nad bannerem, ale bylo by potřeba ještě provést dodatečné úpravy v CSS, aby se nedeformovala.

soubor test-300x600(1).html je upraven, aby byl správně responzivní i artikl aside.

Odpovědi na otázky z teoretické části:

1. https je první část, která označuje použitý protokol
   test je část domény dřetího řádu, může se použít například pro zobrazení vašich stránek v jiném jazyce - www.eng.r2b2.cz
   r2b2 je doména druhého řádu, nejdůležitější část url, jelikož se zde nachází např. název vaší společnosti, něco co chcete aby si lidé pamatovali, např. alza atd.
   cz je doména prvního řádu, mohou být státní jako je právě cz, nebo např. sk, de a nebo mezinárodní com a net.
   a1 je adresář, kde se nachází soubory.
   b2 je název dokumentu
   ?article=12&widgets[]=a&widgets[]=b#foo označuje použití nějakých widgetů na webu.

2. Selektory podle HTML tagů, např. <h2> budou ovlivňovat vzhled všech h2 na stránce. Můžeme také použít class=name a přiřadit jej pouze k určitým částem webu (div, span), které chceme
ovlivnit, nebo můžeme použít i id  (opět pro div a span)  , které může být ale pouze jedno a tudíž se nám poté nemůže stát že bychom ovlivnili nechtěně i ostatní části. Symbol hvězdičky (*) bude ovlivňovat vše,
kde jej umístíme, což se může hodit při nějakém stylování, které chceme přidat ke všem částem a nemusíme upravovat celé CSS.Pseudoselektor jako je například :hover bude části webu ovlivňovat jen za určité situace,
zde konkrétně pokud najedeme kurzorem myši například na odkaz, který má v CSS nastavený i hover a:hover.

3. Musím na daném prohlížeči svůj web otestovat a ideálně se podívat na zdrojový kód, já používám aktuálně hlavně Operu a zde stačí pravým tlačítkem kliknout a zvolit Revize prvku stránky. Při dané funkci
mohu snadno odlišovat jednotlivé části webu a samozřejmě zde uvidím i chyby, když se něco nenačetlo, nebo se to načetlo špatně.

Doufám že odpovědi vám takto budou stačit a také že jsem odpověděl snad správně. Budu se těšit na vaší odpověď.